require("dotenv").config();

const nodeExternals = require("webpack-node-externals");
const NodemonPlugin = require('nodemon-webpack-plugin');
const path = require('path');

module.exports = {
  entry: "./src/index.ts",
  externals: [ nodeExternals() ],
  mode: process.env.NODE_ENV,
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          "ts-loader",
        ],
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
  },
  plugins: [
    new NodemonPlugin()
  ],
  resolve: {
    extensions: [".ts", ".js"],
    modules: [
      path.resolve("./src"),
      path.resolve(".")
    ]
  },
  target: "node",
  watch: process.env.NODE_ENV === "development",
}