enum TYPES {
  // Room related
  ROOM_JOIN = "room:join",
  ROOM_STATE_UPDATE = "room:update",
  PLAYER_LIST_UPDATE = "players:update",
  // Chat related
  CHAT_SEND = "chat:send",
  CHAT_RECEIVE = "chat:receive",
  // Game related
  GAME_START_COUNTDOWN = "game:start-countdown",
  GAME_START = "game:start",
  GAME_ANSWER = "game:answer",
  GAME_RESET_TIMER = "game:reset-timer",
  GAME_INCORRECT = "game:incorrect",
  GAME_USER_INPUT = "game:user-input",
  GAME_CHANGE_TURN_SPEED = "game:change-turn-speed",
  GAME_CHANGE_STARTING_LIVES = "game:change-starting-lives",
  GAME_GET_POINTS = "game:get-points",
  // Error related
  ERROR_USERNAME = "error:username",
  ERROR_IN_GAME = "error:ingame",
}

export default TYPES;
