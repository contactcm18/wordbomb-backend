import { getRandomWord, isWord } from "lib/randomWord";
import { IPlayer, ISyncedGameState, IPrivateGameState } from "./types";

class Room {
  private _maxPlayers = 6;
  constructor(private _id: string, private _owner: string) {}

  get id(): string {
    return this._id;
  }

  get owner(): string {
    return this._owner;
  }

  get currentTurnId() {
    return this._syncedGameState.currentTurn.userId;
  }

  get players(): IPlayer[] {
    return this._players;
  }

  private _players: IPlayer[] = [];

  // Game state that is sent to client
  private _syncedGameState: ISyncedGameState = {
    owner: this._owner,
    started: false,
    currentTurn: {
      index: -1,
      userId: "",
    },
    settings: {
      nextTurnSpeed: 15, // in seconds
      startingLives: 3,
    },
    currentChallenge: "",
    turnsTakenOnChallenge: 0,
    lives: {},
    scores: {},
  };

  // Game state that is not sent to client
  private _privateGameState: IPrivateGameState = {
    alivePlayers: 0,
  };

  get syncedGameState() {
    return this._syncedGameState;
  }

  private _settingsInfo = {
    minNextTurnSpeed: 1,
    maxNextTurnSpeed: 30,
    minStartingLives: 1,
    maxStartingLives: 8,
    pointsPerLetter: 80,
  };

  public addPlayer = (player: IPlayer) => {
    if (
      this._syncedGameState.started ||
      this._players.length >= this._maxPlayers
    )
      return {
        success: false,
        message: "You cannot join the game at this time.",
      };

    if (this._players.find((p) => p.authId === player.authId))
      return { success: false, message: "You cannot join multiple times." };

    this._players.push(player);
    this._syncedGameState.lives[
      player.socketId
    ] = this._syncedGameState.settings.startingLives;
    this._syncedGameState.scores[player.socketId] = 0;

    // Emit new players array
    this.onUpdatePlayerList(this._players);

    // Emit new game state
    this.onUpdateGameState(this._syncedGameState);

    return {
      success: true,
      message: "Player has joined successfully.",
    };
  };

  public removePlayer = (socketId: string) => {
    const i = this._players.findIndex((p) => p.socketId === socketId);
    if (i === -1)
      return {
        success: false,
        message: "Failed to remove player. They do not exist.",
      };

    // Check to change owner of game
    const changeOwner = this._players[i].socketId === this._owner;

    this._players.splice(i, 1);
    this._privateGameState.alivePlayers--;
    delete this._syncedGameState.lives[socketId];
    delete this._syncedGameState.scores[socketId];

    if (changeOwner && this._players[0]) {
      this._owner = this._players[0].socketId;
      this._syncedGameState.owner = this._owner;
      this.onUpdateGameState(this._syncedGameState);
    }

    this.onUpdatePlayerList(this._players);
  };

  private _answerTimer = null;
  private startAnswerTimer = () => {
    if (this._answerTimer) clearTimeout(this._answerTimer);

    this.onStartAnswerTimer();

    // Start timer
    this._answerTimer = setTimeout(() => {
      // Remove 1 life from user
      this._syncedGameState.lives[
        this._syncedGameState.currentTurn.userId
      ] -= 1;
      // If player died, remove 1 from turnsTakenOnChallenge
      if (
        !this._syncedGameState.lives[this._syncedGameState.currentTurn.userId]
      ) {
        this._privateGameState.alivePlayers--;
        this._syncedGameState.turnsTakenOnChallenge--;
      }

      if (this.checkForEndGame()) {
        this.changeTurn(true);
        this.onUpdateGameState(this._syncedGameState);
        return;
      }

      // Set turn to next user
      this.changeTurn();

      this._syncedGameState.turnsTakenOnChallenge++;

      // If the challenge has gone through every player
      if (
        this._syncedGameState.turnsTakenOnChallenge >=
        this._privateGameState.alivePlayers
      ) {
        this._syncedGameState.turnsTakenOnChallenge = 0;
        this.changeChallenge(false);
      } else {
        this.startAnswerTimer();
      }

      this.onUpdateGameState(this._syncedGameState);
    }, this._syncedGameState.settings.nextTurnSpeed * 1000);
  };

  private checkForEndGame = () => {
    // If all but one players have no lives, end game
    let noLives: number = 0;
    for (let value of Object.values(this._syncedGameState.lives)) {
      if (!value) noLives++;
    }

    if (noLives >= this._players.length - 1) {
      // End game
      this._syncedGameState.started = false;
      this.onUpdateGameState(this._syncedGameState);
      return true;
    }

    return false;
  };

  private _countdownTime = 5000;
  public startGame = async (withCountdown: boolean = true) => {
    if (this._syncedGameState.started)
      return {
        success: false,
        message: "Game is already started.",
      };

    if (this._players.length < 2)
      return {
        success: false,
        message: "There must be at least 2 players to start the game.",
      };

    if (withCountdown) this.onStartGame(this._countdownTime);
    // If there's already a challenge in display, remove it
    if (this._syncedGameState.currentChallenge) {
      this._syncedGameState.currentChallenge = "";
      this.onUpdateGameState(this._syncedGameState);
    }
    // Wait for countdown timer
    if (withCountdown)
      await new Promise((resolve, reject) =>
        setTimeout(() => resolve(true), this._countdownTime)
      );

    this._syncedGameState.started = true;
    this._syncedGameState.currentTurn.index = 0;
    this._syncedGameState.currentTurn.userId = this._players[
      this._syncedGameState.currentTurn.index
    ].socketId;
    this._privateGameState.alivePlayers = this._players.length;

    this.resetPlayerPoints();
    this.setPlayerLivesTo(this._syncedGameState.settings.startingLives);

    this.changeChallenge();

    return {
      success: true,
      message: "Game started successfully.",
    };
  };

  public onSubmitAnswer = (answer: string, userId: string) => {
    if (!answer || this._syncedGameState.currentTurn.userId !== userId)
      return {
        success: false,
        message: "Not a valid answer.",
      };

    const filteredAnswer = answer.toLowerCase().trim();

    const includesChallenge = filteredAnswer.includes(
      this._syncedGameState.currentChallenge
    );
    if (!includesChallenge || !isWord(filteredAnswer))
      return {
        success: false,
        message: "Not a valid answer.",
      };

    this._syncedGameState.turnsTakenOnChallenge = 0;

    // Add points to player
    const points = filteredAnswer.length * this._settingsInfo.pointsPerLetter;
    this._syncedGameState.scores[userId] += points;
    this.onPlayerGetPoints(userId, points);

    // Change turn to next player
    this.changeTurn();

    // Get new challenge
    this.changeChallenge(false);

    // Send updated game state
    this.onUpdateGameState(this._syncedGameState);

    return {
      success: true,
      message: "Answer is correct!",
    };
  };

  public changeChallenge = (forceUpdate: boolean = true) => {
    // Get new challenge
    const challengeSize = 3;
    const word = getRandomWord(challengeSize);

    if (word.length <= challengeSize)
      this._syncedGameState.currentChallenge = word;
    // If word is long enough, get a random sequence of 3 letters from the word
    else {
      const startIndex = Math.floor(
        Math.random() * (word.length - challengeSize)
      );
      this._syncedGameState.currentChallenge = word.substring(
        startIndex,
        startIndex + challengeSize
      );
    }

    // Emit to players
    if (forceUpdate) this.onUpdateGameState(this._syncedGameState);

    this.startAnswerTimer();
  };

  private changeTurn = (toNone: boolean = false) => {
    if (toNone) {
      this._syncedGameState.currentTurn = { index: 0, userId: "" };
      return;
    }

    let currentIndex = this._syncedGameState.currentTurn.index;
    let newIndex = 0;
    // Find next player that does not have 0 lives
    do {
      newIndex =
        currentIndex === this._players.length - 1 ? 0 : currentIndex + 1;
      currentIndex = newIndex;
    } while (
      this._syncedGameState.lives[this._players[newIndex].socketId] === 0
    );

    this._syncedGameState.currentTurn = {
      index: newIndex,
      userId: this._players[newIndex].socketId,
    };

    this.onChangeTurn();
  };

  public setTurnSpeed = (value: number) => {
    if (
      !this.numMeetsRequirements(
        value,
        this._settingsInfo.minNextTurnSpeed,
        this._settingsInfo.maxNextTurnSpeed
      )
    )
      return;

    this._syncedGameState.settings.nextTurnSpeed = value;
    this.onUpdateGameState(this._syncedGameState);
  };

  public setStartingLives = (value: number) => {
    if (
      !this.numMeetsRequirements(
        value,
        this._settingsInfo.minStartingLives,
        this._settingsInfo.maxStartingLives
      )
    )
      return;

    this._syncedGameState.settings.startingLives = value;
    this.setPlayerLivesTo(value);
    this.onUpdateGameState(this._syncedGameState);
  };

  private numMeetsRequirements = (
    num: number,
    minValue: number,
    maxValue: number
  ) => typeof num === "number" && num >= minValue && num <= maxValue;

  private setPlayerLivesTo = (value: number) => {
    for (let key of Object.keys(this._syncedGameState.lives)) {
      this._syncedGameState.lives[key] = value;
    }
  };

  private resetPlayerPoints = () => {
    for (let key of Object.keys(this._syncedGameState.scores)) {
      this._syncedGameState.scores[key] = 0;
    }
  };

  private onStartGame = (countdownTime: number) => {};
  public setOnStartGame = (callback: (countdownTime: number) => void) =>
    (this.onStartGame = callback);

  private onUpdateGameState = (newGameState: ISyncedGameState) => {};
  public setOnUpdateGameState = (
    callback: (newGameState: ISyncedGameState) => void
  ) => (this.onUpdateGameState = callback);

  private onPlayerGetPoints = (socketId: string, points: number) => {};
  public setOnPlayerGetPoints = (
    callback: (socketId: string, points: number) => void
  ) => (this.onPlayerGetPoints = callback);

  private onUpdatePlayerList = (newPlayerList: IPlayer[]) => {};
  public setOnUpdatePlayerList = (
    callback: (newPlayerList: IPlayer[]) => void
  ) => (this.onUpdatePlayerList = callback);

  private onStartAnswerTimer = () => {};
  public setOnStartAnswerTimer = (callback: () => void) =>
    (this.onStartAnswerTimer = callback);

  private onChangeTurn = () => {};
  public setOnChangeTurn = (callback: () => void) =>
    (this.onChangeTurn = callback);
}

export default Room;
