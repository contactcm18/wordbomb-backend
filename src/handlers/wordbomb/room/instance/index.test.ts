import Room from ".";
import { IPlayer } from "./types";

describe("Room functionalities", () => {
  const ROOM_ID = "abcdef";
  const FIRST_PLAYER: IPlayer = {
    authId: "zzfdaz",
    socketId: "fhdajns83",
    username: "charl1",
  };

  const room = new Room(ROOM_ID, FIRST_PLAYER.socketId);

  it("should be setup correctly", () => {
    expect(room.id).toBe(ROOM_ID);
    expect(room.owner).toBe(FIRST_PLAYER.socketId);
  });

  it("should add player correctly", () => {
    room.addPlayer({ ...FIRST_PLAYER });

    // Check that player is added
    expect(room.players[0]).toMatchObject(FIRST_PLAYER);
    // Check that player has correct # of lives
    expect(room.syncedGameState.lives[FIRST_PLAYER.socketId]).toBe(
      room.syncedGameState.settings.startingLives
    );
    // Check that player has 0 score
    expect(room.syncedGameState.scores[FIRST_PLAYER.socketId]).toBe(0);
    // Check that player is the owner
    expect(room.owner).toBe(FIRST_PLAYER.socketId);
    expect(room.syncedGameState.owner).toBe(FIRST_PLAYER.socketId);
  });

  it("should remove player correctly", () => {
    const newPlayer: IPlayer = {
      authId: "abde",
      socketId: "12hfd8342k",
      username: "charl2",
    };

    room.addPlayer({ ...newPlayer });
    room.removePlayer(FIRST_PLAYER.socketId);

    // Host should transfer to newly added player
    expect(room.syncedGameState.owner).toBe(newPlayer.socketId);
    expect(room.owner).toBe(newPlayer.socketId);
    expect(room.players).toHaveLength(1);
    expect(room.players[0]).toMatchObject(newPlayer);
    expect(room.syncedGameState.lives[FIRST_PLAYER.socketId]).toBeUndefined();
    expect(room.syncedGameState.scores[FIRST_PLAYER.socketId]).toBeUndefined();
  });

  it("should not start game with 1 player", async () => {
    const { success } = await room.startGame(false);
    expect(success).toBe(false);
  });

  it("should start game correctly with >1 players", async () => {
    room.addPlayer({ ...FIRST_PLAYER });

    const { success } = await room.startGame(false);
    expect(success).toBe(true);

    expect(room.syncedGameState.started).toBe(true);
    expect(room.syncedGameState.currentTurn).toMatchObject({
      index: 0,
      userId: room.players[0].socketId,
    });

    // Check that all players have correct # lives
    for (const key of Object.keys(room.syncedGameState.lives)) {
      expect(room.syncedGameState.lives[key]).toBe(
        room.syncedGameState.settings.startingLives
      );
    }
    // Check that all players have correct # points
    for (const key of Object.keys(room.syncedGameState.scores)) {
      expect(room.syncedGameState.scores[key]).toBe(0);
    }

    expect(room.syncedGameState.currentChallenge).toBeTruthy();
  });

  it("should change turn after correct answer", () => {
    // Game is already started from above test
    // Rig it so we can answer correctly
    const challenge = "rigged";
    room.syncedGameState.currentChallenge = challenge;

    const { success } = room.onSubmitAnswer(
      challenge,
      room.players[0].socketId
    );
    expect(success).toBe(true);

    expect(room.syncedGameState.currentChallenge).not.toBe(challenge);
    expect(room.syncedGameState.currentTurn).toMatchObject({
      index: 1,
      userId: room.players[1].socketId,
    });
  });
});

export default null;
