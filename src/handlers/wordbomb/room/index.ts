import { Server, Socket } from "socket.io";
import { decode } from "jsonwebtoken";
import uniqid from "uniqid";

import { syncVerify, AUTH_TOKEN_KEY, generateToken } from "routes/auth/helpers";
import Room from "./instance";
import types from "../types";

const liveRooms: { [roomId: string]: Room } = {};

const verifyAuthentication = (socket: Socket) => {
  const { token } = socket.handshake.auth;
  let { cookie } = socket.handshake.headers;
  if (!cookie && !token) return "";

  // Test user does not have token, grant one
  if (process.env.SOCKET_AUTH && token === process.env.SOCKET_AUTH)
    return generateToken("test_user", uniqid());

  // Add ; to end of cookie for easier matching
  cookie += ";";
  const value = cookie.match(new RegExp(`${AUTH_TOKEN_KEY}=([^;]+)`));
  if (!value || !value[1]) return "";

  return syncVerify(value[1]) ? value[1] : "";
};

/**
 * Setup specific room events
 * @param room
 * @param io
 */
const registerRoomEvents = (room: Room, io: Server) => {
  room.setOnStartGame((countdownTime: number) => {
    io.to(room.id).emit(types.GAME_START_COUNTDOWN, countdownTime);
  });

  room.setOnUpdatePlayerList((newPlayerList) =>
    io.to(room.id).emit(types.PLAYER_LIST_UPDATE, newPlayerList)
  );

  room.setOnUpdateGameState((newGameState) =>
    io.to(room.id).emit(types.ROOM_STATE_UPDATE, newGameState)
  );

  room.setOnStartAnswerTimer(() => io.to(room.id).emit(types.GAME_RESET_TIMER));

  room.setOnChangeTurn(() => {
    io.to(room.id).emit(types.GAME_USER_INPUT, "");
  });

  room.setOnPlayerGetPoints((socketId, points) => {
    io.to(socketId).emit(types.GAME_GET_POINTS, points);
  });
};

/**
 * Registers events that listen to client emits
 * @param room Room instance this player is in
 * @param socket
 * @param io
 */
const registerSocketEvents = (room: Room, socket: Socket, io: Server) => {
  socket.on("disconnect", () => {
    if (room) room.removePlayer(socket.id);
  });

  socket.on(types.GAME_START, () => {
    if (socket.id === room.owner) room.startGame();
  });

  socket.on(types.GAME_USER_INPUT, (value: string) => {
    if (socket.id === room.currentTurnId)
      io.to(room.id).emit(types.GAME_USER_INPUT, value);
  });

  socket.on(types.GAME_ANSWER, (answer: string) => {
    const { success } = room.onSubmitAnswer(answer, socket.id);
    if (!success) io.to(socket.id).emit(types.GAME_INCORRECT);
  });

  socket.on(types.GAME_CHANGE_STARTING_LIVES, (value: number) =>
    room.setStartingLives(value)
  );

  socket.on(types.GAME_CHANGE_TURN_SPEED, (value: number) =>
    room.setTurnSpeed(value)
  );
};

const roomHandlers = (io: Server, socket: Socket) => {
  const joinOrCreateRoom = (roomId: string) => {
    const token: string = verifyAuthentication(socket);
    if (!token) {
      io.to(socket.id).emit(types.ERROR_USERNAME);
      return;
    }

    // Decode the user's token
    const { username, id } = decode(token, { json: true });

    let _roomId: string = roomId;
    // If the room does not exist, set one up
    if (_roomId && !io.sockets.adapter.rooms.get(_roomId)) {
      _roomId = roomId || uniqid();

      // Create room and setup its events
      const room = new Room(_roomId, socket.id);

      registerRoomEvents(room, io);

      // Save to liveRooms array
      liveRooms[room.id] = room;
    }

    socket.join(_roomId);
    registerSocketEvents(liveRooms[_roomId], socket, io);

    // Server events
    io.of("/").adapter.on("delete-room", (room) => delete liveRooms[room]);

    // Attempt to join room
    const { success } = liveRooms[_roomId].addPlayer({
      socketId: socket.id,
      authId: id,
      username,
    });
    if (!success) io.to(socket.id).emit(types.ERROR_IN_GAME);
  };

  socket.on(types.ROOM_JOIN, joinOrCreateRoom);
};

export default roomHandlers;
