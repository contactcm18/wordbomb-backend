import { Server, Socket } from "socket.io";

import types from "../types";

interface IMessage {
  username: string;
  text: string;
  id: string;
}

const chatHandlers = (io: Server, socket: Socket) => {
  const sendMessage = (roomId: string, message: IMessage) => {
    const socketInRoom = io.sockets.adapter.rooms.get(roomId).has(socket.id);
    // Ensure at least one char is not whitespace
    const isWhitespace = !/\S/.test(message.text);
    if (!socketInRoom || isWhitespace) return;

    io.to(roomId).emit(types.CHAT_RECEIVE, message);
  };

  socket.on(types.CHAT_SEND, sendMessage);
};

export default chatHandlers;
