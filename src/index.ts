import { config } from "dotenv";
config();

import express from "express";
import cors from "cors";
import { createServer } from "http";
import { Server, Socket } from "socket.io";

const app = express();
const httpServer = createServer(app);
const corsOptions: cors.CorsOptions = {
  origin: process.env.CLIENT_HOST.includes("localhost")
    ? /localhost/
    : process.env.CLIENT_HOST,
  methods: ["GET", "POST"],
  credentials: true,
};

const io = new Server(httpServer, {
  cors: corsOptions,
});

const PORT = process.env.PORT || "4000";
httpServer.listen(PORT, () => {
  console.log(`Server up on port ${PORT}.`);
});

// Middleware

app.use(cors(corsOptions));

// Routes

import authRoute from "routes/auth";
app.use("/auth", authRoute);

// io handlers

import registerWordbombRoomHandlers from "handlers/wordbomb/room";
import registerWordbombChatHandlers from "handlers/wordbomb/chat";

const onConnection = (socket: Socket) => {
  registerWordbombRoomHandlers(io, socket);
  registerWordbombChatHandlers(io, socket);
};

io.on("connection", onConnection);

export { app };
