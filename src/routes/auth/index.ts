import { Router, json } from "express";
import cookieParser from "cookie-parser";
import { decode } from "jsonwebtoken";

import {
  generateToken,
  isUsernameValid,
  syncVerify,
  attachAuthTokenCookie,
} from "./helpers";

const router = Router();
router.use(json());
router.use(cookieParser());

router.post("/setUsername", (req, res) => {
  const { username } = req.body;
  const { valid, messages } = isUsernameValid(username);
  if (!valid) return res.status(400).json(messages);

  const { auth_token } = req.cookies;
  const verified = syncVerify(auth_token);

  const newToken = generateToken(
    username,
    // Will return current id or undefined
    (() => {
      if (verified) {
        const decodedToken = decode(auth_token, { json: true });
        return decodedToken.id;
      }
    })()
  );

  attachAuthTokenCookie(res, newToken);
  return res.sendStatus(200);
});

export default router;
