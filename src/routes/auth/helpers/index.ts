import { Response } from "express";
import { verify, sign } from "jsonwebtoken";
import uniqid from "uniqid";

const MIN_USERNAME_LENGTH = 3;
const MAX_USERNAME_LENGTH = 18;
export const isUsernameValid = (username: string) => {
  const valid: boolean =
    username &&
    username.length >= MIN_USERNAME_LENGTH &&
    username.length <= MAX_USERNAME_LENGTH &&
    /\S/.test(username);

  const messages = valid
    ? null
    : [
        `Username must be between ${MIN_USERNAME_LENGTH} and ${MAX_USERNAME_LENGTH} characters long`,
      ];

  return {
    valid,
    messages,
  };
};

export const syncVerify = (token: string) => {
  try {
    if (!token) throw Error("Token does not exist.");
    verify(token, process.env.JWT_AUTH_SECRET);
    return true;
  } catch (error) {
    return false;
  }
};

export interface IAuthToken {
  id: string;
  username: string;
}

export const generateToken = (username: string, id?: string) => {
  const payload: IAuthToken = {
    id: id || uniqid(),
    username,
  };
  return sign(payload, process.env.JWT_AUTH_SECRET);
};

export const AUTH_TOKEN_KEY = "auth_token";
export const attachAuthTokenCookie = (res: Response, cookie: string) => {
  res.cookie(AUTH_TOKEN_KEY, cookie, {
    httpOnly: true,
    expires: new Date(253402300000000),
    secure: process.env.HOST.includes("https"),
  });
};
