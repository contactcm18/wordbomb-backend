require("dotenv").config();
import { isUsernameValid, syncVerify, generateToken } from ".";

describe("auth helpers functionalities", () => {
  test("invalid usernames", () => {
    expect(isUsernameValid("a").valid).toBe(false);
    expect(isUsernameValid("ab").valid).toBe(false);
    expect(isUsernameValid("asuperreallylongnamethatistoolong").valid).toBe(
      false
    );
  });

  test("valid usernames", () => {
    expect(isUsernameValid("abc").valid).toBe(true);
    expect(isUsernameValid("charlie12").valid).toBe(true);
    expect(isUsernameValid("a_longer_username").valid).toBe(true);
    expect(isUsernameValid("a name with spaces").valid).toBe(true);
  });

  it("should be an invalid token", () => {
    let token = generateToken("charl12", "abcda9ds") + "_invalid";
    expect(syncVerify(token)).toBe(false);
  });

  it("should generate a valid token", () => {
    const token = generateToken("charl12", "abcda9ds");
    expect(syncVerify(token)).toBe(true);
  });
});
