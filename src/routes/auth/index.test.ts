require("dotenv").config();
import request from "supertest";
import { decode } from "jsonwebtoken";

import { app } from "src/index";
import { AUTH_TOKEN_KEY, syncVerify } from "./helpers";

describe("POST /setUsername", () => {
  const USERNAME = "charlie";

  test("as new user", (done) => {
    // Send a request as a new user
    request(app)
      .post("/auth/setUsername")
      .send({ username: USERNAME })
      .end((err, res) => {
        if (err) return done(err);

        const cookie: string = res.headers["set-cookie"][0];
        expect(cookie).toContain(AUTH_TOKEN_KEY);

        // Verify the token
        const token = getTokenFromCookie(cookie);
        expect(syncVerify(token)).toBe(true);

        // Decode and check values
        const { username } = decode(token, { json: true });
        expect(username).toBe(USERNAME);

        return done();
      });
  });

  test("as existing user", async (done) => {
    try {
      const NEW_USERNAME = "panther";

      // Make the initial request as a new user
      let res = await request(app)
        .post("/auth/setUsername")
        .send({ username: USERNAME });

      let token = getTokenFromCookie(res.headers["set-cookie"][0]);
      const { id: originalId } = decode(token, { json: true });

      // As an existing user, make another request to change username. Will have same cookies as above
      res = await request(app)
        .post("/auth/setUsername")
        .set("Cookie", res.headers["set-cookie"])
        .send({ username: NEW_USERNAME });

      token = getTokenFromCookie(res.headers["set-cookie"][0]);
      const { username, id } = decode(token, { json: true });

      // Check that id remained the same
      expect(originalId).toBe(id);
      // Check that username changed to wanted name
      expect(username).toBe(NEW_USERNAME);

      return done();
    } catch (error) {
      return done(error);
    }
  });
});

const getTokenFromCookie = (cookie: string): string =>
  cookie.match(/=(.*?);/)[1];
