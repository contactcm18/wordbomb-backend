import { getRandomWord, isWord } from ".";

describe("randomWord library functionalities", () => {
  it("should generate a random word to constraints", () => {
    expect(getRandomWord(4).length).toBeGreaterThanOrEqual(4);
    expect(getRandomWord(6).length).toBeGreaterThanOrEqual(6);
    expect(getRandomWord(8).length).toBeGreaterThanOrEqual(8);
    expect(getRandomWord(10).length).toBeGreaterThanOrEqual(10);
  });

  test("invalid words", () => {
    expect(isWord("abrachadabrah")).toBe(false);
    expect(isWord("abcde")).toBe(false);
    expect(isWord("hurrry")).toBe(false);
    expect(isWord("aweesome")).toBe(false);
    expect(isWord("deniall")).toBe(false);
  });

  test("valid words", () => {
    expect(isWord(getRandomWord(3))).toBe(true);
    expect(isWord(getRandomWord(4))).toBe(true);
    expect(isWord(getRandomWord(5))).toBe(true);
    expect(isWord(getRandomWord(6))).toBe(true);
  });
});
