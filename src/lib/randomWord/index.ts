import words from "static/wordbomb/words_dictionary";

/**
 *
 * @param minLength min length of the wanted word.
 * @returns a string word.
 */
export const getRandomWord = (minLength: number) => {
  let word = "";
  do {
    word = words[Math.floor(Math.random() * words.length)];
  } while (word.length < minLength);
  return word;
};

/**
 * Executes a binary search on hundreds of thousands of words.
 * @param word
 * @returns true or false, whether the word exists
 */
export const isWord = (word: string) => {
  let start = 0;
  let end = words.length;

  do {
    let midIndex = Math.floor((start + end) / 2);
    if (words[midIndex] === word) return true;

    if (word < words[midIndex]) end = midIndex - 1;
    else start = midIndex + 1;
  } while (start <= end);

  return false;
};
